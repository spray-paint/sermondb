const App = () => {
  const [sermons, setSermons] = React.useState([]);

  React.useEffect(() => {
    // fetch('')
    // .then(res => res.json())
    // .then(json => setSermons(json));
  }, []);

  return (
    <React.Fragment>

      <h1>SERMONS</h1>
    
      <ul className="nav nav-tabs mb-3">
        <li className="nav-item">
          <a href="#" className="nav-link active">ALL</a>
        </li>
        <li className="nav-item">
          <a href="#" className="nav-link">TOPICS</a>
        </li>
        <li className="nav-item">
          <a href="#" className="nav-link">SCRIPTURE</a>
        </li>
        <li className="nav-item">
          <a href="#" className="nav-link">SERIES</a>
        </li>
        <li className="nav-item">
          <a href="#" className="nav-link">DATE</a>
        </li>
      </ul>

      <div className="row g-3">
        <div className="col-md-10">
        <input type="text" className="form-control" placeholder="Search..." />
        </div>
        <div className="col">
          <select className="form-select">
            <option>Newest</option>
            <option>Oldest</option>
            <option>Most Viewed</option>
          </select>
        </div>
      </div>

      <ul className="list-group list-group-flush">
        {sermons.map(item => <li className="list-group-item"></li>)}
      </ul>
      
    </React.Fragment>
  );
};

const root = document.getElementById('app');
ReactDOM.render(<App />, root);