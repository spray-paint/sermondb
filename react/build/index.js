var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var App = function App() {
  var _React$useState = React.useState([]),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      sermons = _React$useState2[0],
      setSermons = _React$useState2[1];

  React.useEffect(function () {
    // fetch('')
    // .then(res => res.json())
    // .then(json => setSermons(json));
  }, []);

  return React.createElement(
    React.Fragment,
    null,
    React.createElement(
      "h1",
      null,
      "SERMONS"
    ),
    React.createElement(
      "ul",
      { className: "nav nav-tabs mb-3" },
      React.createElement(
        "li",
        { className: "nav-item" },
        React.createElement(
          "a",
          { href: "#", className: "nav-link active" },
          "ALL"
        )
      ),
      React.createElement(
        "li",
        { className: "nav-item" },
        React.createElement(
          "a",
          { href: "#", className: "nav-link" },
          "TOPICS"
        )
      ),
      React.createElement(
        "li",
        { className: "nav-item" },
        React.createElement(
          "a",
          { href: "#", className: "nav-link" },
          "SCRIPTURE"
        )
      ),
      React.createElement(
        "li",
        { className: "nav-item" },
        React.createElement(
          "a",
          { href: "#", className: "nav-link" },
          "SERIES"
        )
      ),
      React.createElement(
        "li",
        { className: "nav-item" },
        React.createElement(
          "a",
          { href: "#", className: "nav-link" },
          "DATE"
        )
      )
    ),
    React.createElement(
      "div",
      { className: "row g-3" },
      React.createElement(
        "div",
        { className: "col-md-10" },
        React.createElement("input", { type: "text", className: "form-control", placeholder: "Search..." })
      ),
      React.createElement(
        "div",
        { className: "col" },
        React.createElement(
          "select",
          { className: "form-select" },
          React.createElement(
            "option",
            null,
            "Newest"
          ),
          React.createElement(
            "option",
            null,
            "Oldest"
          ),
          React.createElement(
            "option",
            null,
            "Most Viewed"
          )
        )
      )
    ),
    React.createElement(
      "ul",
      { className: "list-group list-group-flush" },
      sermons.map(function (item) {
        return React.createElement("li", { className: "list-group-item" });
      })
    )
  );
};

var root = document.getElementById('app');
ReactDOM.render(React.createElement(App, null), root);