'use strict';

const fs = require('fs');
const request = require('request');
const cheerio = require('cheerio');

const RSS_URL = 'http://www.gracefellowshipcc.org/sermons/?podcast';
const ITUNES_URL = 'itpc://www.gracefellowshipcc.org/sermons/?podcast';
const BROWSER_URL = 'http://www.gracefellowshipcc.org/sermons/?pagenum=';

for (let page = 1; page < 76; page++) {
    request.get(BROWSER_URL + page, (error, response) => {
        if (response.statusCode !== 200) {
            console.error('Non-200 response returned from url attempting to query ' + BROWSER_URL + page);
            process.exit(1);
        }

        let $ = cheerio.load(response.body);
        const titles = $('.sermon-title');
        const passageAndSeries = $('.sermon-passage');
        const audio = $('audio');
        const preacher = $('.preacher');

        for (let i = 0; i < titles.length; i++) {
            const current = {};
            current.pageNumber = page;
            current.itemNumber = i;

            current.title = titles.eq(i).text();

            const passageSeriesBlob = passageAndSeries.eq(i).text();
            const {groups: {passage, series}} = /^(?<passage>.*)\(Part of the (?<series>.*) series\)/.exec(passageSeriesBlob);
            current.passage = passage.trim();
            current.series = series.trim();

            const preacherBlob = preacher.eq(i).text();
            const {groups: {author, date, serviceType}} = /^Preached by (?<author>.*) on (?<date>.*) \((?<serviceType>.*)\)/.exec(preacherBlob);
            current.author = author;
            current.date = date;
            current.serviceType = serviceType;

            const audioBlob = audio.eq(i).text();
            try {
                if (audioBlob) {
                    const {groups: {url}} = /\=(?<url>.*)/.exec(audioBlob);
                    current.file = wrapSermonFileInUrl(decodeURIComponent(url));
                }
            } catch(err) {
                console.log('error parsing url: ' + audio.eq(i).text() + ', for message titled: ' + current.title + ', on date: ' + current.date + ', page ' + page);
            }

            //results.push(current);
            fs.appendFile("raw_sermons.json", JSON.stringify(current) + "\n", 'utf8', function (err) {
                if (err) {
                    console.log("An error occured while writing JSON Object to File.");
                    return console.log(err);
                }
            });

        }
    });
}

/**
 * The file names are coming in two different ways:
 *
 * 2008-06-15.mp3
 * http://sermons.gracefellowshipcc.org/2014-07-06.mp3
 *
 * @return normalized sermon name with http address
 */
function wrapSermonFileInUrl(sermonFile) {
    return sermonFile.indexOf('http') < 0 ? `http://sermons.gracefellowshipcc.org/${sermonFile}` : sermonFile;
}
